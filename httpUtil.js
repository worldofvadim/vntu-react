import store from './store/store';
import * as firebase from 'firebase';

export function getHeaders(){
  return {
    'Authorization':store.getState().login.accessToken,
    'Content-Type':'application/json'
  }
}

let init = false;

export function getFirebase(){
  if (!init){
  var config = {
      apiKey: "AIzaSyAnhAABAeUd8VYjaydm5l5dsGs_M9yUSFg",
      authDomain: "test-cbf1b.firebaseapp.com",
      databaseURL: "https://test-cbf1b.firebaseio.com",
      projectId: "test-cbf1b",
      storageBucket: "test-cbf1b.appspot.com",
      messagingSenderId: "905830360929"
    };
    firebase.initializeApp(config);
    init = true;
    }
    return firebase;

}
