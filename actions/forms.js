import {getFirebase} from '../httpUtil';
import { storeAccessToken, updateTodoList } from './index';
import {remoteUrl} from '../constants';
import {getHeaders} from '../httpUtil';
import {Alert} from 'react-native';

const firebase = getFirebase();

export const onLogin = (values, dispatch, props) => {
  const {email, password} = values;
  return firebase.auth().signInWithEmailAndPassword(email, password)
  .then((resp)=>resp.user.getIdToken())
  .then(token=>{
    dispatch(storeAccessToken(token));
    dispatch(updateTodoList());
    props.navigation.replace('Home');
  })
  .catch(error=>{
    console.log(error);
    Alert.alert('Error Authentication');
  })
}


export const onAddTodo = (values, dispatch, props) => {
  const todo = values.todo;

  console.log(todo);
  if(!todo){
    Alert.alert('Todo must be filled');
    return;
  }

  fetch(remoteUrl,{
    method:'POST',
    headers: getHeaders(),
    body: JSON.stringify({text:todo})
  })
  .then(resp=>{
    dispatch(updateTodoList());
    props.navigation.goBack();
  })
  .catch(error=>{
    console.log(error);
    throw new SubmissionError({
      _error:{loginError: error}
    });
  })
}
