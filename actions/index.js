import { ACCESS_TOKEN, DELETE_TODO, ADD_TODO, UPDATE_TODO_LIST, UPDATE_TODO, START_SPINNER, STOP_SPINNER } from '../constants';
import {Alert} from 'react-native';


import {remoteUrl} from '../constants';
import {getHeaders} from '../httpUtil';


export const storeAccessToken = (token) => {
  return {
    type: ACCESS_TOKEN,
    payload: token
  }
}

export const deleteTodoItem = (data) => {
  return (dispatch) => {
    fetch(remoteUrl + '?id=' + data.id, {
      method:'DELETE',
      headers: getHeaders(),
    })
    .then(resp=>{
      dispatch({
        type: DELETE_TODO,
        payload: data
      });
    })
    .catch(e=> {
      Alert.alert('Error during delete of item')
    });
 };
}

export const updateTodoList = () => {
  return (dispatch) => {
    dispatch({type:START_SPINNER});
    fetch(remoteUrl, {
      headers: getHeaders()
    })
    .then((response) => response.json())
    .then((responseJson) => {
      dispatch({type:STOP_SPINNER});
      dispatch({type:UPDATE_TODO_LIST, payload:responseJson});
    })
    .catch(error=> {
      Alert.alert(error);
      dispatch({type:STOP_SPINNER});
    })
    .finnaly;
 };
}


export const updateTodoItem = (item) => {
  item.done = !item.done;
   return (dispatch) => {fetch(remoteUrl, {
     method:'PUT',
     headers: getHeaders(),
     body: JSON.stringify(item)
   })
   .then((resp) => resp.json())
   .then((responseJson) => {
     dispatch({type:UPDATE_TODO, payload: responseJson});
   })
   .catch(error=> Alert.alert(error));
  };

}
