import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import rootReducer from '../reducers';

const initialState = {
  todo:{
    todos:[
      {text:'Save the world'},
      {text:'Wash clothes'},
      {text:'Cook diner'},
      {text:'Meditate'},
    ],
    update:false,
  }
};

const middleware = [thunk];

const store = createStore(rootReducer, initialState, applyMiddleware(...middleware));

export default store;
