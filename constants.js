export const ACCESS_TOKEN = 'ACCESS_TOKEN';

export const remoteUrl = 'http://35.198.125.112:8080/api/todo';

export const ADD_TODO = 'ADD_TODO';
export const UPDATE_TODO = 'UPDATE_TODO';
export const DELETE_TODO = 'DELETE_TODO';
export const UPDATE_TODO_LIST = 'UPDATE_TODO_LIST';
export const CREATE_TODO_LIST = 'CREATE_TODO';
export const LOGOUT = 'LOGOUT';
export const START_SPINNER = 'START_SPINNER';
export const STOP_SPINNER = 'STOP_SPINNER';
