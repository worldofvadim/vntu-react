import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createStackNavigator } from 'react-navigation'
import { Provider } from 'react-redux';

import store from './store/store';
import DashboardScreen from './screens/DashboardScreen';
import LoginScreen from './screens/LoginScreen';
import {HomeAndAbout} from './screens/HomeScreen';
import AboutScreen from './screens/AboutScreen';
import AddTodoScreen from './screens/AddTodoScreen';
import RegistrationScreen from './screens/RegistrationScreen';


export default class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
        <AppStackNavigator />
      </Provider>
    );
  }
}


const AppStackNavigator = createStackNavigator({
  Dashboard: DashboardScreen,
  Home: {
    screen:HomeAndAbout,
    navigationOptions: {header: null}
  },
  Login: LoginScreen,
  AddTodo: AddTodoScreen,
  About: AboutScreen,
  Registration: RegistrationScreen
})
