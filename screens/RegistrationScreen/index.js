import React from 'react';
import {
  View,
  Text,
  KeyboardAvoidingView,
  StyleSheet
} from 'react-native';
import Logo from '../../components/Logo';
import RegistrationForm from '../../components/forms/RegistrationForm';
import {styles} from './style';

export default class RegistrationScreen extends React.Component{


  static navigationOptions = {
    title: 'Registration',
    headerStyle: {
      backgroundColor: '#845ec2',
    },
    headerTitleStyle: {
      color:'#b0a8b9'
    },
  }

  render(){
    return(
      <View style={styles.container}>
        <KeyboardAvoidingView style={styles.contentWrapper} behavior="padding" enabled>
        <Logo />
        <RegistrationForm {...this.props}/>
      </KeyboardAvoidingView>
    </View>
    );
  }
}
