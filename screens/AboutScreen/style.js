import {StyleSheet, StatusBar} from 'react-native';


export const styles = StyleSheet.create({
  container:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'#4b4453',
  },
  text:{
    textAlign:'center',
    fontSize:22,
    color:'#ff8066',
  }
});
