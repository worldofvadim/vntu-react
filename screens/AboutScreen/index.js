import React from 'react';
import {
  View,
  Text,
  StyleSheet
} from 'react-native';
import {styles} from './style';

export default class AboutScreen extends React.Component{

  static navigationOptions = {
    header: {
    visible: false,
  }
  }

  render(){
    return(
      <View style={styles.container}>
        <Text style={styles.text}>This is application is developed with React Native</Text>
        <Text style={styles.text}>In application you can write your tasks and check them when you are done</Text>
        <Text style={styles.text}>Author is student of VNTU</Text>
        <Text style={styles.text}>Vadim Ozarinskiy</Text>
        <Text style={styles.text}>2018</Text>
      </View>
    );
  }
}
