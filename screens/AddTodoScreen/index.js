import React from 'react';
import {
  View,
  Text,
  StyleSheet
} from 'react-native';
import Logo from '../../components/Logo';
import AddTodoForm from '../../components/forms/AddTodoForm';
import {styles} from './style';

export default class AddTodoScreen extends React.Component{

  static navigationOptions = {
    title: 'Add todo',
    headerStyle: {
      backgroundColor: '#845ec2',
    },
    headerTitleStyle: {
      color:'#b0a8b9'
    },
  }

  render(){
    return(
      <View style={styles.container}>
        <AddTodoForm {...this.props}/>
      </View>
    );
  }
}
