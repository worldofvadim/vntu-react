import {StyleSheet, StatusBar} from 'react-native';


export const styles = StyleSheet.create({
  container:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
    backgroundColor:'#4b4453',
    paddingTop:StatusBar.currentHeight,
  },
  buttonContainer:{
    flexDirection:'column',
    justifyContent:'flex-start',
    alignSelf:'flex-start',
    width:80,
    margin:20,
  },
});
