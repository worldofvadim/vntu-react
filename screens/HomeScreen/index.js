import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Button,
  StatusBar
} from 'react-native';
import store from '../../store/store';
import * as Actions from '../../constants';
import TodoList from '../../components/TodoList';
import Icon from 'react-native-vector-icons/Ionicons'; // 4.2.0
import ActionButton from 'react-native-action-button'; // 2.7.2
import {styles} from './style';
import AboutScreen from '../AboutScreen';
import { createMaterialTopTabNavigator } from 'react-navigation';

class HomeScreen extends React.Component{

  constructor(props){
    super(props);
  }

  static navigationOptions = {
    header: null
  }

  render(){
    console.log(store.getState().login.accessToken);
    return(
      <View style={styles.container}>

      <TodoList />
        <View style={styles.buttonContainer}>
          <Button
          containerViewStyle={styles.button}
          color='#845ec2'
          title="Exit"
          onPress={()=>{
            store.dispatch({type:Actions.LOGOUT});
            this.props.navigation.navigate('Dashboard');
          }}
          />
        </View>

        <ActionButton buttonColor="rgba(231,76,60,1)"
            onPress={() => this.props.navigation.navigate('AddTodo')}>
        </ActionButton>

      </View>
    );
  }
}

export const HomeAndAbout = createMaterialTopTabNavigator({
  Home: HomeScreen,
  About: AboutScreen,
},{
  tabBarOptions: {
      style: {
        paddingTop: StatusBar.currentHeight,
        backgroundColor:'#845ec2'
    }
  }
});
