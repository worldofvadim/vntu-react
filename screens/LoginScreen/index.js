import React from 'react';
import {
  View,
  Text,
  KeyboardAvoidingView,
  StyleSheet
} from 'react-native';
import Logo from '../../components/Logo';
import LoginForm from '../../components/forms/LoginForm';
import {styles} from './style';

export default class LoginScreen extends React.Component{

  static navigationOptions = {
    title: 'Login',
    headerStyle: {
      backgroundColor: '#845ec2',
    },
    headerTitleStyle: {
      color:'#b0a8b9'
    },
  }

  constructor(props){
    super(props);
    this.props = props;
  }

  render(){
    return(
      <View style={styles.container}>
        <KeyboardAvoidingView style={styles.contentWrapper}  behavior="padding" enabled>
            <Logo {...this.props}/>
            <LoginForm {...this.props}/>
        </KeyboardAvoidingView>
      </View>
    );
  }
}
