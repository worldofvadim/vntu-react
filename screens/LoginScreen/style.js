import {StyleSheet} from 'react-native';


export const styles = StyleSheet.create({
  container:{
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor:'#b0a8b9',
  },
  contentWrapper:{
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    height:'70%',
    width:'80%',
  }
});
