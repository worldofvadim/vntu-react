import {StyleSheet} from 'react-native';


export const styles = StyleSheet.create({
  titleContainer:{
    alignItems:'center',
    width:'80%',
    borderWidth:2,
    borderColor:'#ff8066',
  },
  titleText:{
    marginTop:10,
    textAlign:'center',
    fontSize:22,
    color:'#ff8066',
  },
  container:{
    flex:1,
    alignItems:'center',
    justifyContent:'space-evenly',
    backgroundColor:'#4b4453',
  },
  contentContainer:{
    height:200,
    alignItems:'center',
    justifyContent:'space-evenly',
    width:'80%'
  },
  contentRow:{
    borderWidth:2,
    borderColor:'#b0a8b9',
    alignItems:'center',
    justifyContent:'space-between',
    margin:0,
    width:'100%',
    flexDirection:'row',
    // backgroundColor:'yellow'
  },
  buttonContainer:{
    flexDirection:'row',
    alignItems:'flex-end',
    justifyContent:'space-between',
    width:'80%',
    height:200,
  },
  buttons:{
    width:100,
  },
  text:{
    fontSize:20,
    color:'#b0a8b9',
  },
  checkmark:{
    width:20,
    height:20
  }
});
