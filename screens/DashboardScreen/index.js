import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Button,
  Animated,
  Image
} from 'react-native';
import TypeWriter from 'react-native-typewriter';
import {styles} from './style';

class FadeInView extends React.Component {

  state = {
    fadeAnim: new Animated.Value(0),  // Initial value for opacity: 0
  }

  componentDidMount() {
    Animated.timing(                  // Animate over time
      this.state.fadeAnim,            // The animated value to drive
      {
        toValue: 1,                   // Animate to opacity: 1 (opaque)
        duration: 1000,
        delay:2000,                    // Make it take a while
      }
    ).start();                        // Starts the animation
  }

  render() {
    let { fadeAnim } = this.state;

    return (
      <Animated.View                 // Special animatable View
        style={{
          ...this.props.style,
          opacity: fadeAnim,         // Bind opacity to animated value
        }}
      >
        {this.props.children}
      </Animated.View>
    );
  }
}

export default class DashboardScreen extends React.Component{

  static navigationOptions = {
      header: null
    }

  render(){
    return(
      <View style={styles.container}>

      <View style={styles.titleContainer}>
        <Text style={styles.titleText}> Write all your tasks in one place and check them when done! </Text>
      </View>

      <View style={styles.contentContainer}>
        <View style={styles.contentRow}>
          <TypeWriter
          style={styles.text}
          typing={1}
          minDelay={100}
          > Save the world!</TypeWriter>
          <FadeInView>
            <Image
            style={styles.checkmark}
            source={require('../../assets/checkmark.gif')}
            />
          </FadeInView>
        </View>
        <View style={styles.contentRow}>
          <TypeWriter
          style={styles.text}
          typing={1}
          minDelay={30}
          > Cook dinner</TypeWriter>
          <FadeInView>
            <Image
            style={styles.checkmark}
            source={require('../../assets/checkmark.gif')}
            />
          </FadeInView>
        </View>
        <View style={styles.contentRow}>
          <TypeWriter
          style={styles.text}
          typing={1}
          minDelay={80}
          > Wash clothes</TypeWriter>
          <FadeInView>
            <Image
            style={styles.checkmark}
            source={require('../../assets/checkmark.gif')}
            />
          </FadeInView>
        </View>
        <View style={styles.contentRow}>
          <TypeWriter
          style={styles.text}
          typing={1}
          > Get some sleep</TypeWriter>
        </View>
      </View>
      <View style={styles.buttonContainer}>
        <View style={styles.buttons}>
            <Button
            color='#845ec2'
            title="Login"
            onPress={()=>this.props.navigation.navigate('Login')}
            />
          </View>
          <View style={styles.buttons}>
            <Button
            color="#845ec2"
            title="Register"
            onPress={()=>this.props.navigation.navigate('Registration')}
            />
            </View>
        </View>
      </View>
    );
  }
}
