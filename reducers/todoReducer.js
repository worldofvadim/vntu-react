import * as Actions from '../constants';

const initialState = {
  todos: []
}

export default function(state = initialState, action) {
  const {todos, ...props} = state;

  switch (action.type) {
    case Actions.START_SPINNER:
      return {...state, update: true};

    case Actions.STOP_SPINNER:
      return {...state, update: false};

    case Actions.UPDATE_TODO:
      let newTodo = [...todos];
      let index = findIndexForTodo(action.payload, todos);
      newTodo[index] = action.payload;
      return{
        ...props,
        todos:newTodo
      };

    case Actions.UPDATE_TODO_LIST:
      return {...props, todos:action.payload};

    case Actions.DELETE_TODO:
      return {
        ...props,
        todos: todos.filter(todo => todo.id !== action.payload.id)
      };

    case Actions.LOGOUT:
      return {
        ...props,
        todos: []
      }

    default:
      return state;
  }
};

function findIndexForTodo(item, array){
  for (var i = 0; i < array.length; i++) {
    if(array[i].id === item.id)
      return i;
  }
  return -1;
}
