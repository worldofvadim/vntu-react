import * as Actions from '../constants';

const initialState = {
}

export default function(state = initialState, action) {
  switch (action.type) {
    case Actions.ACCESS_TOKEN:
      return{
        ...state,
        accessToken: action.payload
      }
    case Actions.LOGOUT:
      return {

      }
    default:
      return state;
  }
};

function logout(state){
  const {accessToken, ...props} = state;
  return {
    ...props,
    accessToken: ''
  }
}
