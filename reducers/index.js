import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import loginReducer from './loginReducer';
import todoReducer from './todoReducer';

export default combineReducers({
  login: loginReducer,
  form: formReducer,
  todo: todoReducer
})
