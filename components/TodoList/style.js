import {StyleSheet} from 'react-native';


export const styles = StyleSheet.create({
  container:{
    // backgroundColor:'red',
    width:'80%',
  },
  todo:{
    textAlign:'center',
    fontSize:22,
    color:'#ff8066',
  }
});
