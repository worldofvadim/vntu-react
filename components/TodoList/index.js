import React from 'react';
import { connect } from 'react-redux';
import {Button, ScrollView, TouchableNativeFeedback, Text, Alert,RefreshControl} from 'react-native';
import CheckBox from 'react-native-checkbox';
import store from '../../store/store';
import {remoteUrl} from '../../constants';
import {getHeaders} from '../../httpUtil';
import * as Constants from '../../constants';
import * as Actions from '../../actions';
import {styles} from './style';

class TodoList extends React.Component{
  constructor(props){
    super(props);
    this.props = props;
  }

  // <Button
  // title='update'
  // onPress={()=>this.props.updateTodoList()}
  // />
  render(){
    return (
      <ScrollView
      style={styles.container}
      refreshControl={
          <RefreshControl
            refreshing={this.props.update}
            onRefresh={()=>this.props.updateTodoList()}
          />
        }
      >
      {this.props.todos.map((l,i)=>(
        <TouchableNativeFeedback
        key={i}
        accessibilityComponentType="button"
        delayLongPress={1000}
        onLongPress={() => this.props.deleteItem(l)}
        onPress={() => this.props.updateTodoItem(l)}
        >
        <Text style={[ l.done ? {textDecorationLine: 'line-through'} : {}, styles.todo]}>{l.text}</Text>
        </TouchableNativeFeedback>
      ))}
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    todos: state.todo.todos,
    update: state.todo.update
  }
};


const mapDispatchToProps = (dispatch) => {
  return {
    deleteItem: (item) => dispatch(Actions.deleteTodoItem(item)),
    updateTodoItem: (item) => dispatch(Actions.updateTodoItem(item)),
    updateTodoList: () => dispatch(Actions.updateTodoList()),
  }
};


export default connect(mapStateToProps, mapDispatchToProps)(TodoList)
