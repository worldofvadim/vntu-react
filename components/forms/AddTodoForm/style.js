import {StyleSheet} from 'react-native';


export const styles = StyleSheet.create({
  container:{
    flex:1,
    justifyContent:'flex-start',
    width:'80%',
    margin:20,
  },
  todo:{
    color: '#ff8066',
    fontWeight: 'bold',
    fontSize: 18,
    height:200,
  }
});
