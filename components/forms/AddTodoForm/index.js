import React, { Component } from 'react';
import {Button, View, TextInput, Text, Alert} from 'react-native';
import {reduxForm, Field, SubmissionError} from 'redux-form';
import CustomTextInput from '../../CustomTextInput';
import {onAddTodo} from '../../../actions/forms';
import {styles} from './style';

class AddTodoForm extends Component{

  constructor(props){
    super(props);
    this.props = props;
  }

  render(){
    return (
      <View style={styles.container}>
        <Field
        style={styles.todo}
        name={'todo'}
        placeholder={'Type your todo here ...'}
        component={CustomTextInput}
        />

        <Button
        title="Submit"
        color='#845ec2'
        onPress={this.props.handleSubmit}
        />
      </View>
    )
  }
}

let emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export default reduxForm({
  form: 'addTodo',
  onSubmit: onAddTodo
})(AddTodoForm)
