import React, { Component } from 'react';
import {Button, View, TextInput, Text, Alert} from 'react-native';
import {reduxForm, Field, SubmissionError} from 'redux-form';
import {NavigationActions} from 'react-navigation'
import CustomTextInput from '../../CustomTextInput';
import {storeAccessToken} from '../../../actions';
import {getFirebase} from '../../../httpUtil';
import {styles} from './style';

const firebase = getFirebase();



class RegistrationForm extends Component{

  constructor(props){
    super(props);
    this.props = props;
  }

  render(){
    return (
      <View style={styles.container} behavior="padding" enabled>

        {
          this.props.error !== undefined && (this.props.error.loginError !== undefined &&
            <Text
            style={{color: 'red'}}
            >Authentication failed</Text>)
        }

        <Field
          name={'email'}
          placeholder={'E-mail'}
          style={{height:40}}
          component={CustomTextInput}
        />

        <Field
          name={'password'}
          placeholder={'password'}
          secureTextEntry={true}
          style={{height:40}}
          component={CustomTextInput}
        />

        <Field
          name={'confirmPassword'}
          placeholder={'Confirm password'}
          secureTextEntry={true}
          style={{height:40}}
          component={CustomTextInput}
        />

        <Button
        title="Submit"
        color='#845ec2'
        onPress={this.props.handleSubmit}
        />
      </View>
    )
  }
}

let emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export default reduxForm({
  form: 'register',
  onSubmit: (values, dispatch, props) => {
    const {email, password} = values;
    return firebase.auth().createUserWithEmailAndPassword(email, password)
    .then(token=>{
      props.navigation.replace('Login');
    })
    .catch(error=>{
      console.log(error);
      throw new SubmissionError({
        _error:{loginError: error}
      });
    })
  },
  validate: (values) => {
    const errors = {};
    console.log(values);
    errors.email = !values.email
      ? 'Email field is required'
      : !emailRegex.test(values.email)
      ? 'email forat is invalid'
      : undefined;

    errors.password = !values.password
      ? 'Password field is requred'
      : values.password.length < 8
      ? 'Password must be at least 8 characters long'
      : undefined;

    errors.confirmPassword = !values.confirmPassword
      ? 'Confirm password is required'
      : values.password != values.confirmPassword
      ? 'Passwords are not equial'
      : undefined;

    return errors;
  }
})(RegistrationForm)
