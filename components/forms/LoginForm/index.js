import React, { Component } from 'react';
import {Button, View, TextInput, Text, Alert} from 'react-native';
import {reduxForm, Field} from 'redux-form';
import {NavigationActions} from 'react-navigation'
import CustomTextInput from '../../CustomTextInput';
import {storeAccessToken, updateTodoList} from '../../../actions';
import { onLogin } from '../../../actions/forms';
import {styles} from './style';

class LoginForm extends Component{

  constructor(props){
    super(props);
    this.props = props;
  }

  render(){
    return (
        <View style={styles.container}>

          {
            this.props.error !== undefined && (this.props.error.loginError !== undefined &&
              <Text
              style={{color: 'red'}}
              >Authentication failed</Text>)
          }

          <Field
            name={'email'}
            placeholder={'E-mail'}
            style={{height:40}}
            component={CustomTextInput}
          />



          <Field
            name={'password'}
            placeholder={'password'}
            secureTextEntry={true}
            style={{height:40}}
            component={CustomTextInput}
          />

          <Button
          title="Submit"
          color='#845ec2'
          onPress={this.props.handleSubmit}
          />
          <View style={{height:40}} />
        </View>
    )
  }
}

let emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;



export default reduxForm({
  form: 'signIn',
  onSubmit: onLogin,
  validate: (values) => {
    const errors = {};
    errors.email = !values.email
      ? 'Email field is required'
      : !emailRegex.test(values.email)
      ? 'email forat is invalid'
      : undefined;

    errors.password = !values.password
      ? 'Password field is requred'
      : values.password.length < 8
      ? 'Password must be at least 8 characters long'
      : undefined;

    return errors;
  }
})(LoginForm)
