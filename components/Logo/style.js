import {StyleSheet} from 'react-native';


export const styles = StyleSheet.create({
  image:{
    resizeMode:'stretch',
    height: 80,
    width: 250,
    borderWidth: 1,
  }
});
