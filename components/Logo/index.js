import React, {Component} from 'react';
import {Image, View} from 'react-native';
import {styles} from './style';

class Logo extends Component {
  render(){
    return (
      <View>
      <Image
         style={styles.image}
         source={require('../../assets/logo.png')}
       />
       </View>
    );
  }
}

export default Logo;
