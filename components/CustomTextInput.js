import React from 'react';
import {TextInput, View, Text } from 'react-native';


export default function CustomTextInput(props){
  const { input, meta, ...inputProps } = props;

  const formStates = ['active', 'autofilled', 'asyncValidating', 'dirty', 'invalid', 'pristine',
    'submitting', 'touched', 'valid', 'visited'];

  return (
    <View>
      <TextInput
        {...inputProps}
        onChangeText={input.onChange}
        onBlur={input.onBlur}
        onFocus={input.onFocus}
        value={input.value}
      />
      {meta.touched && (meta.error && <Text style={{color: 'red'}}>{meta.error}</Text>)}
    </View>
);
}
